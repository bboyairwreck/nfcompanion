!(function() {
    var patientID = localStorage.getItem("patient");
    var firstName = localStorage.getItem("firstName");
    var lastName = localStorage.getItem("lastName");
    //var profile = JSON.parse(localStorage.getItem("profile"));
    $(document).ready(function() {
        var fullName = localStorage.getItem("profile");
        $("#profileName").text(fullName);
        $("#fullName").text(fullName);
        $("#dob").text(localStorage.getItem("dob"));
        $("#phone").text(localStorage.getItem("phone"));
        //var address = profile["address"];
        $("#addressStreet").text(localStorage.getItem("street"));
        $("#addressCity").text(localStorage.getItem("cityState"));
        $("#emergencyName").text(localStorage.getItem("emergencyName"));
        $("#emergencyPhone").text(localStorage.getItem("emergencyPhone"));
        $("#doctor").text(localStorage.getItem("doctor"));
        $("#hospital").text(localStorage.getItem("hospital"));
        $("#blood").text(localStorage.getItem("blood"));
        var allergies = localStorage.getItem("allergies");
        var allergiesArr = allergies.split(", ");
        for (var i = 0; i < allergiesArr.length; i++) {
            $("#allergies").append("<p>"+allergiesArr[i]+"</p>");
        }
        var meds = localStorage.getItem("meds");
        var medsArr = meds.split(", ");
        for (var i = 0; i < medsArr.length; i++) {
            $("#meds").append("<p>"+medsArr[i]+"</p>");
        }
        $("#insurance").text(localStorage.getItem("insurance"));
        $("#ssn").text(localStorage.getItem("ssn"));


        $("#editProfileButton").click(function() {

        });


        //$("#editButton").click(function(){
        //    $name = $("#main").text();
        //    $("#editMain").text($name);
        //    $("input[name='name']").prop('value', $name);
        //    $dob = $("#birthdate").text();
        //    $("input[name='dob']").prop('value', $dob);
        //    $address = $("#address").text();
        //    $("input[name='address']").prop('value', $address);
        //    $citystate = $("#citystate").text();
        //    $("input[name='citystate']").prop('value', $citystate);
        //    $phone = $("#phone").text();
        //    $("input[name='phone']").prop('value', $phone);
        //    $emergency = $("#emergency").text();
        //    $("input[name='emergency']").prop('value', $emergency);
        //    $doctor = $("#doctor").text();
        //    $("input[name='doctor']").prop('value', $doctor);
        //    $hospital = $("#hospital").text();
        //    $("input[name='hospital']").prop('value', $hospital);
        //    $blood = $("#blood").text();
        //    $("input[name='blood']").prop('value', $blood);
        //    $allergies = "";
        //    $("#allergies li").each(function(index) {
        //        $allergies = $allergies + $(this).text() + " ";
        //    });
        //    alert($allergies);
        //    $("input[name='allergies']").prop('value', $allergies);
        //    $meds = "";
        //    $("#meds li").each(function(index) {
        //        $meds = $meds + $(this).text() + " ";
        //    });
        //    alert($meds);
        //    $("input[name='meds']").prop('value', $meds);
        //    $insurance = $("#insurance").text();
        //    $("input[name='insurance']").prop('value', $insurance);
        //});
        //
        //// update profile information
        //$("#submitProfile").click(function() {
        //    $setName = $("input[name='name']").prop('value');
        //    $("#main").text($setName);
        //    $setDob = $("input[name='dob']").prop('value');
        //    $("#birthdate").text($setDob);
        //    $setAddress = $("input[name='address']").prop('value');
        //    $("#address").text($setAddress);
        //    $setCityState = $("input[name='citystate']").prop('value');
        //    $("#citystate").text($setCityState);
        //    $setPhone = $("input[name='phone']").prop('value');
        //    $("#phone").text($setPhone);
        //    $setEmergency = $("input[name='emergency']").prop('value');
        //    $("#emergency").text($setEmergency);
        //    $setDoctor = $("input[name='doctor']").prop('value');
        //    $("#doctor").text($setDoctor);
        //    $setHospital = $("input[name='hospital']").prop('value');
        //    $("#hospital").text($setHospital);
        //    $setBlood = $("input[name='blood']").prop('value');
        //    $("#blood").text($setBlood);
        //    $setAllergies = $("input[name='allergies']").prop('value');
        //    $("#allergies").text($setAllergies);
        //    $setMeds = $("input[name='meds']").prop('value');
        //    $("#meds").text($setMeds);
        //    $setInsurance = $("input[name='insurance']").prop('value');
        //    $("#insurance").text($setInsurance);
        //});
    });
}());