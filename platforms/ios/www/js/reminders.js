!(function(){
    var patientID = localStorage.getItem("patient");
    $(document).ready(function() {

        var url = "http://ericchee.com/neverforgotten/getEvents_Patient.php";

        $.ajax(url, {
            dataType : "json",
            data : {
                'n' : patientID
            },
            success : ajaxSuccess,
            error : ajaxError
        });


        $('body').on('touchend', '.reminderCard', function() {
            var curEventID = $(this).attr("data-eventID");
            var eventInfo = [];
            eventInfo["eventID"] = curEventID;
            params.push(eventInfo);
        });

    });

    function addEventParam() {
        //var eventInfo = [];
        //eventInfo["eventID"] = 1234;
        //params.push(eventInfo);
        params["eventID"] = 1234;
    }

    function ajaxSuccess(data) {

        for (var i = 0; i < data.length; i++){
            var $newCard = $("#taskTemplate .reminderCard").clone();

            var evTitle = data[i]["EventTitle"];
            var evDatetime = data[i]["EventTime"];
            var evDatetimeArr = datetimeFormat(evDatetime);
            var evDate = evDatetimeArr["date"];


            var evTime = evDatetimeArr["time"];
            var evTimeFormatted = timeFormat(evTime);
            var evEventID = data[i]["EventID"];

            var dateFormatArr = dateFormat(evDate);

            // Check if date is today
            var dayString = dateFormatArr["dateLine"];
            if (evDate == getCurDateString()) {
                dayString = "Today";
            }



            // Get DayWrapper Card
            var $dayWrap = $('.dayWrap[data-date="'+ evDate + '"]');    // look for dayWrapper for specific date
            var $dayHeader;
            var $dayCardWrap;
            if ($dayWrap.length > 0) {
                // found date wrapper
                $dayHeader = $dayWrap.find(".dayHeader");
                $dayCardWrap = $dayWrap.find(".dayCardWrap");

            } else {
                // didnt find date wrapper
                $dayWrap = $('<div class="dayWrap">');
                $dayWrap.attr("data-date", evDate);

                $dayHeader = $('<h4 class="dayHeader">');
                $dayHeader.text(dayString);

                $dayCardWrap = $('<div class="dayCardWrap">');

                $dayWrap.append($dayHeader);
                $dayWrap.append($dayCardWrap);

                $('#reminderList').prepend($dayWrap);
            }

            //  dateFormatArr["dayName"] + dateFormatArr["monthName"]

            // inject any thing inside of $newTask;
            var $eventTitle = $newCard.find(".eventTitle");
            $eventTitle.html(evTitle);

            var $eventTime = $newCard.find(".reminderTime");
            $eventTime.text(evTimeFormatted);
            $newCard.attr("data-eventID", evEventID);


            $dayCardWrap.prepend($newCard);
        }

    }

    function getCurDateString() {
        var nowDate = new Date();
        var monthString = "" + (nowDate.getMonth() + 1);
        var dayString = "" + nowDate.getDate();

        if ((nowDate.getMonth() + 1) < 10) {
            monthString = "0" + "" + monthString;
        }

        if (nowDate.getDate() < 10) {
            dayString = "0" + "" + dayString;
        }

        var nowDateString = nowDate.getFullYear() + "-" + monthString + "-" + dayString;

        return nowDateString;
    }

    function ajaxError( xhr, status, errorThrown ) {
        alert( "Sorry, there was an Ajax problem!" );
        console.log( "Error: " + errorThrown );
        console.log( "Status: " + status );
        console.dir( xhr );
    }

    $("#callButton").on('touchend', function(){
        document.addEventListener("deviceready", function(){

            var patID = localStorage.getItem("patient");
            // TODO need to add FName and LName of Companion
            //var patFName = localStorage.getItem("firstName");
            //var patLName = localStorage.getItem("lastName");

            //alert("patID:" + patID);

            // get Firebase to Person we're calling
            var fire = new Firebase('https://neverforgotten.firebaseio.com/');
            var patCallLogFire = fire.child(patID+ "/callLog");


            var perIDOfComp = localStorage.getItem("personCompanion");
            // push to firebase - send your (patient's) id along with w
            patCallLogFire.push({
                companion:perIDOfComp
            });
            // TODO need to add FName and LName of companion

            navWithParams("call.html");

        }, false);
    })

}());