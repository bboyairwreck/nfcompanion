!(function() {
    var personCompID = localStorage.getItem("personCompanion");
    var personPatientID = localStorage.getItem("personPatient");
    var patientName = localStorage.getItem("firstName");
    $(document).ready(function() {
        $("#patientName").text(patientName);

        $("select.selectRepeat").change(function() {
            var remTime = $("select.selectNot option:selected").text();
            $("#alertTime").text(remTime);
            $("#alertTime").removeClass("promptTime");
        });

        $("#addAlertButton").click(function() {
            var alertTime = $("#alertTime").text();
            var alertTimeArr = alertTime.split(" ");
            var alertNum = alertTimeArr[0];
            var alertType = alertTimeArr[1];
            var alertPerson;

            if (!$("#addToMe").hasClass('active') && !$("#addToPatient").hasClass('active')) {
                alert("Select a person to be notified to continue.");
            } else if ($("#alertTime").hasClass("promptTime")) {
                alert("Select a time to be notified to continue.");
            } else {
                if ($("#addToMe").hasClass('active') && $("#addToPatient").hasClass('active')) {
                    alertPerson = personCompID + " " + personPatientID;
                } else if ($("#addToMe").hasClass('active')) {
                    alertPerson = personCompID;
                } else if ($("#addToPatient").hasClass('active')) {
                    alertPerson = personPatientID;
                }
                if (params.length > 1) {
                    params.pop();
                }
                var alertInfo = [];
                alertInfo["alertNum"] = alertNum;
                alertInfo["alertType"] = alertType;
                alertInfo["alertPerson"] = alertPerson;
                params.push(alertInfo);

                var href = $(this).data("href");    // page location
                navWithParams(href);
            }
        });

        $("#cancelAlert").click(function() {
            if (params.length > 1) {
                params.pop();
            }
            var href = $(this).data("href");    // page location
            navWithParams(href);
        });
    });
}());
