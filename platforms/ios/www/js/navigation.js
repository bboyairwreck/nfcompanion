$(document).ready(function() {
    checkPage();
});

window.addEventListener('push', checkPage);
var pageStack = [];

function getPageName() {
    var content = document.getElementsByClassName("content")[0];
    var pageName = content.id;
    return pageName;
}

function checkPage() {
    pageStack.push(getPageName());
    
    var content = document.getElementsByClassName("content")[0];
    var pageName = content.id;

    if(pageName) {


        var jsFileName = "js/" + pageName + ".js";

        getJavascript(jsFileName);
        getJavascript('js/notifications.js');
        getJavascript('js/firebase.js');
        loadMenu();

        var cssFileName = "css/" + pageName +".css";
        var fileref = document.createElement("link");
        fileref.setAttribute("rel", "stylesheet");
        fileref.setAttribute("type", "text/css");
        fileref.setAttribute("href", cssFileName);

        if (typeof fileref != "undefined") {
            document.getElementsByTagName("head")[0].appendChild(fileref);
        }
    }
}

function getJavascript(jsFileName) {
    $.getScript(jsFileName)

    .done(function( pageName, textStatus ) {
        console.log( textStatus );
    })

    .fail(function( jqxhr, statusText, errorThrown ) {
        console.log(errorThrown);
        console.log(statusText);
        console.log(jqxhr);
    });
}

var params = [];

function getParams() {
    if (params.length > 0){
        return params[params.length - 1];
    } else {
        return null;
    }
}

function getFirstParam() {
    if (params.length > 0) {
        return params[0];
    } else {
        return null;
    }
}

function getParamsAtIndex(index) {
    if (params.length > 0 && params.length > index) {
        return params[index];
    } else {
        return null;
    }
}

function clearParams() {
    while (params.length > 0) {
        params.pop();
    }
}

function loadMenu(){
    var cssFileName = "css/menu.css";
    var fileref = document.createElement("link");
    fileref.setAttribute("rel", "stylesheet");
    fileref.setAttribute("type", "text/css");
    fileref.setAttribute("href", cssFileName);

    if (typeof fileref != "undefined") {
        document.getElementsByTagName("head")[0].appendChild(fileref);
    }

    getJavascript("js/menu.js");

    //$.getScript("js/menu.js")
    //
    //.done(function( pageName, textStatus ) {
    //    console.log( textStatus );
    //})
    //
    //.fail(function( jqxhr, statusText, errorThrown ) {
    //    console.log(errorThrown);
    //    console.log(statusText);
    //    console.log(jqxhr);
    //});
}

/*
 // Example of adding params
     $("#SOME_BUTTON_ID").click(function() {

     // add param data
     var eventInfo = [];
     eventInfo["eventName"] = "George is awesome";
     params.push(eventInfo);

     var href = $(this).data("href");    // page location
     navWithParams(href);
     });
 */
function navWithParams(pageHref) {

    var targetElement = document.getElementById('ghost');

    if (targetElement == null) {
        alert("ghost anchor does not exist");
    }

    targetElement.href = pageHref;

    var evt = document.createEvent('UIEvent');
    evt.initUIEvent('touchend', true, true, window, 1);
    targetElement.dispatchEvent(evt);
}