!(function(){
    var personPatID = localStorage.getItem("personPatient");
    var personCompID = localStorage.getItem("personCompanion");

    $(document).ready(function() {
        var myParams = getParams();

        var eventID = myParams["eventID"];

        clearParams();

        var url = "http://ericchee.com/neverforgotten/getReminders_Event.php";
        $.ajax(url, {
            dataType : "json",
            data: {
                'n': eventID,
                'patient': personPatID,
                'comp': personCompID
            },
            success : ajaxSuccess,
            error : ajaxError
        });

        $("#editReminderNav").click(function() {
            var eventInfo = [];
            eventInfo["eventID"] = eventID;
            params.push(eventInfo)

            var href = $(this).data("href");
            navWithParams(href);
            // get EventID params.push() and navWithParams
        });
    });

    function ajaxSuccess(data) {
        //$("#alerts").empty();
        if (data.length > 0) {

            var eventName = data[0]["EventTitle"];
            var eventDateTime = data[0]["EventTime"];
            var eventDateTimeArr = datetimeFormat(eventDateTime);
            var eventDate  = eventDateTimeArr["date"];
            var eventDateFormatted = dateFormat(eventDate);
            var eventTime = eventDateTimeArr["time"];
            var eventTimeFormatted = timeFormat(eventTime);

            $("div.reminder-title").html(eventName);
            $("#time").text(eventTimeFormatted);
            $("div.day").text(eventDateFormatted["day"]);
            $("div.month").text(eventDateFormatted["monthName"]);

            for (var i = 0; i < data.length; i++){
                var reminderPersonID = data[i]["PersonID"];
                var reminderPerson = data[i]["PersonFName"];
                if (reminderPersonID == personCompID) {
                    reminderPerson = "Me";
                }
                var reminderNum = data[i]["QuantityNum"];
                var reminderType = data[i]["ReminderType"];
                var reminderTime = reminderNum + " " + reminderType;

                if (reminderNum > 0) {
                    var $newReminder = $("#alertTemplate .alertCard").clone();
                    $newReminder.append(reminderTime + "<span class='light pull-right optionDisplay'>" + reminderPerson + "</span>");
                    $("#alerts").append($newReminder);
                }
            }
        }
    }

    function ajaxError( xhr, status, errorThrown ) {
        alert(errorThrown);
        console.log( "Error: " + errorThrown );
        console.log( "Status: " + status );
        console.dir( xhr );
    }
}());
