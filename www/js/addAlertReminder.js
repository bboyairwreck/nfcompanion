!(function() {
    var patientName = localStorage.getItem("firstName");
    $(document).ready(function() {
        $("#patientName").text(patientName);

        $("select.selectRepeat").change(function() {
            var remTime = $("select.selectNot option:selected").text();
            $("#alertTime").text(remTime);
            $("#alertTime").removeClass("promptTime");
        });

        $("#addAlertButton").click(function() {
            var alertTime = $("#alertTime").text();
            var alertTimeArr = alertTime.split(" ");
            var alertNum = alertTimeArr[0];
            var alertType = alertTimeArr[1];

            if (!$("#addToMe").hasClass('active') && !$("#addToPatient").hasClass('active')) {
                alert("Select a person to be notified to continue.");
            } else if ($("#alertTime").hasClass("promptTime")) {
                alert("Select a time to be notified to continue.");
            } else {
                if ($("#addToMe").hasClass('active')) {
                    addParams(alertNum, alertType, "Me");
                }
                if ($("#addToPatient").hasClass('active')) {
                    addParams(alertNum, alertType, patientName);
                }
                var href = $(this).data("href");    // page location
                navWithParams(href);
            }
        });

        $("#cancelAlert").click(function() {
            var href = $(this).data("href");    // page location
            navWithParams(href);
        });
    });

    function addParams(alertNum, alertType, alertPerson) {
        var alertInfo = [];
        alertInfo["action"] = "addAlert";
        alertInfo["alertNum"] = alertNum;
        alertInfo["alertType"] = alertType;
        alertInfo["alertPerson"] = alertPerson;
        params.push(alertInfo);
    }
}());
