!(function() {
    var personCompID = localStorage.getItem("personCompanion");
    var personPatID = localStorage.getItem("personPatient");
    var patientID= localStorage.getItem("patient");
    $(document).ready(function() {
        // set current time and date - value and span text

        if (params.length == 0) {
            newReminder();
            updateDateSpan();
            updateTimeSpan();
        } else {
            updateReminder();
            updateDateSpan();
            updateTimeSpan();
        }

        $("#remTime").change(function () {
            updateTimeSpan();
        });

        $("#remDate").change(function() {
            updateDateSpan();
        });

        $("select.optionClass").change(function() {
            var selected = $(this).children(":selected").text();
            $(this).prev().children(":first").text(selected);
        });

        $("#addReminderAlert").click(function() {
            var name = $("#remName").val().trim();
            var date = $("#remDate").val();
            var time = $("#remTime").val();
            if (name.length == 0) {
                alert("Please enter a reminder name before continuing.");
            } else {
                var alertInfo = [];
                alertInfo["alertName"] = name;
                alertInfo["alertDate"] = date;
                alertInfo["alertTime"] = time;
                params.push(alertInfo);

                var href = $(this).data("href");
                navWithParams(href);
            }
        });

        $("#doneCreateReminder").click(function() {
            var name = $("#remName").val().trim();
            var date = $("#remDate").val();
            var time = $("#remTime").val();
            if (name.length == 0) {
                alert("Please enter a reminder name before continuing.");
            } else if (date == null) {
                alert("Please enter a reminder date before continuing.");
            } else if (time == null) {
                alert("Please enter a reminder time before continuing.");
            } else {
                var datetime = date + " " + time;
                addReminder(name, datetime);

                var href = $(this).data("href");
                navWithParams(href);
            }
        });

        $("#cancelCreate").click(function() {
            clearParams();
        });
    });

    function newReminder() {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1;
        var yyyy = today.getFullYear();

        if (dd < 10) {
            dd = "0" + dd;
        }
        if (mm < 10) {
            mm = "0" + mm;
        }
        var curDate = yyyy + "-" + mm + "-" + dd;
        alert(curDate);

        $("#remDate").val(curDate);

        var hour = today.getHours();
        var min = today.getMinutes();

        if (min < 10) {
            min = "0" + min;
        }
        if (hour < 10) {
            hour = "0" + hour;
        }

        var curTime = hour + ":" + min;
        alert(curTime);
        $("#remTime").val(curTime);
    }

    function addReminder(name, datetime) {
        var url = "http://ericchee.com/neverforgotten/addEventReminder.php";

        $.ajax(url, {
            dataType : "json",
            data : {
                'name': name,
                'time': datetime,
                'id' : patientID
            },
            success : addReminderSuccess,
            error : ajaxError
        });
    }

    function updateTimeSpan() {
        var time = $("#remTime").val();

        var timeString;
        if (time) {
            var arrTime = time.split(":");

            var hours = arrTime[0];
            var mins = arrTime[1];
            var AMorPM = " AM";

            if (hours.charAt(0) == "0") {
                hours = hours.charAt(1);
            }

            if (parseInt(hours) >= 12) {
                AMorPM = " PM";
            }

            hours = parseInt(hours) % 12;
            if (hours == 0) {
                hours = 12;
            }

            timeString = hours + ":" + mins + AMorPM;
        } else {
            $("#remTime").val("00:00");
            timeString = "12:00 AM";
        }

        $('#remTimeString').text(timeString);
    }

    function updateDateSpan() {
        var date = $("#remDate").val();

        if (date) {
            var dateArr = date.split("-");

            var year = dateArr[0];
            var month = dateArr[1];
            var day = dateArr[2];

            if (month.charAt(0) == "0") {
                month = month.charAt(1);
            }

            if (day.charAt(0) == "0") {
                day = day.charAt(1);
            }

            var monthString = getMonthString(month);
        }

        var dateString = monthString + " " + day + ", " + year;
        $("#remDateString").text(dateString);
    }

    function updateReminder() {
        if (params.length > 0) {
            var reminder = getFirstParam();
            $("#remName").val(reminder["alertName"]);
            $("#remDate").val(reminder["alertDate"]);
            $("#remTime").val(reminder["alertTime"]);
            if (params.length > 1) {        // there are alerts
                for (var i = 1; i < params.length; i++) {
                    var alertData = getParamsAtIndex(i);
                    var alertNum = alertData["alertNum"];
                    var alertType = alertData["alertType"];
                    var alertPerson = alertData["alertPerson"];

                    var $newAlert = $("#reminderAlertTemplate .reminderAlertCard").clone();
                    var alertPersonArr = alertPerson.split(" ");
                    var personName = "";
                    if (alertPersonArr.length == 2) {
                        personName = "Me + " + localStorage.getItem("firstName");
                    } else if (alertPersonArr[0] == personCompID) {
                        personName = "Me";
                    } else if (alertPersonArr[0] == personPatID) {
                        personName = localStorage.getItem("firstName");
                    }
                    var $alertTime = $newAlert.find(".alertTime");
                    $alertTime.text(alertNum + " " + alertType);
                    var $alertName = $newAlert.find(".alertName");
                    $alertName.text(personName);
                    $("#alertView").append($newAlert);
                }
            }
        }
    }

    function addReminderSuccess(data) {
        // get Event ID and loop
        var eventID = data["EventID"];

        // check for parameters (AKA alerts)
        while (params.length > 1) {
            var alertData = getParams();
            var alertNum = alertData["alertNum"];
            var alertType = alertData["alertType"];
            var alertPerson = alertData["alertPerson"];
            var alertPersonArr = alertPerson.split(" ");
            if (alertType.charAt(alertType.length-1) != "s") {
                alertType = alertType + "s";
            }
            for (var i = 0; i < alertPersonArr.length; i++) {
                addAlert(eventID, alertNum, alertType, alertPersonArr[i]);
            }
            params.pop();
        }
        params.pop();

        notifyPatientOfReminder(eventID);
    }

    function addAlert(eventID, alertNum, alertType, alertPerson) {
        var url = "http://ericchee.com/neverforgotten/addReminderAlert.php";

        $.ajax(url, {
            dataType : "json",
            data : {
                'event': eventID,
                'num': alertNum,
                'type' : alertType,
                'id' : alertPerson
            },
            success : addAlertSuccess,
            error : ajaxError
        });
    }

    function addAlertSuccess(data) {
        // add to alert list with reminderID stored in data
        //var reminderID = data["ReminderID"];
    }

    function ajaxError( xhr, status, errorThrown ) {
        alert(errorThrown);
        console.log( "Error: " + errorThrown );
        console.log( "Status: " + status );
        console.dir( xhr );
    }

    function notifyPatientOfReminder(evID) {
        var patID = localStorage.getItem("patient");
        var perID = localStorage.getItem("personCompanion");
        var fNameComp = localStorage.getItem("firstNameComp");
        var lNameComp = localStorage.getItem("lastNameComp");

        var fire = new Firebase('https://neverforgotten.firebaseio.com/');
        var patFire = fire.child(patID + "/reminderCreatedComp");
        patFire.push({
            eventID : evID,
            personCompanionID : perID,
            firstNameComp : fNameComp,
            lastNameComp : lNameComp
        });
    }
}());


