var companionID = localStorage.getItem("companion");

!(function() {
    $(document).ready(function(){

        // Firebase - Check for changes
        var fire = new Firebase('https://neverforgotten.firebaseio.com/');
        var patID = localStorage.getItem("patient");
        var patFire = fire.child(patID + "/reminderCreated");

        patFire.on('child_added', fbRemAdded);



    });


    $(".blackShade, .notifDismiss").on("touchend", function() {
        $(".blackShade").fadeOut();
        $(".blackShade").removeClass("open");

        var $notifModal = $(".notificationModal");
        if ($notifModal.hasClass("open")){
            $notifModal.removeClass("open");
        }

        $notifModal.fadeOut();
    });

    $(".notifViewDetails").on("touchend", function() {
        var curEventID = $(this).attr("data-eventID");

        var eventInfo = [];
        eventInfo["eventID"] = curEventID;
        params.push(eventInfo);

        $(".blackShade").fadeOut();
        $(".blackShade").removeClass("open");

        var $notifModal = $(".notificationModal");
        if ($notifModal.hasClass("open")){
            $notifModal.removeClass("open");
        }

        $notifModal.fadeOut();

        navWithParams("viewReminder.html");
    });


    function fbRemAdded(snapshot) {

        var msg = snapshot.val();
        var key = snapshot.key();

        var patID = localStorage.getItem("patient");
        var eventID = msg["EventID"];



        var url = "http://ericchee.com/neverforgotten/getEventInfo.php";  // TODO need getEventInfo
        $.ajax(url, {
            dataType : "json",
            data : {
                'n' : patID,
                'id' : eventID
            },
            success : reminderInfoSuccess,
            error : ajaxErrorRem
        });

    }

    function reminderInfoSuccess(data) {
        var eventName = data["EventTitle"];
        var eventDateTime = data["EventDateTime"];
        var eventDateTimeArr = datetimeFormat(eventDateTime);
        var eventDate  = eventDateTimeArr["date"];
        var eventDateFormatted = dateFormat(eventDate);
        var eventTime = eventDateTimeArr["time"];
        var eventTimeFormatted = timeFormat(eventTime);
        var eventID = data["EventID"];


        var $notifModal = $(".notificationModal");
        $(".notifTitle").text(eventName);
        $(".notifTime").text(eventTimeFormatted);
        $(".notifViewDetails").attr("data-eventID", eventID);
        // TODO use something with eventDateFormatted which is an array

        $notifModal.addClass("open");

        var $blackShade = $(".blackShade");
        $blackShade.fadeIn();

        $notifModal.fadeIn();

        // remove from call log if received
        clearRemAddedLog();
    }

    function clearRemAddedLog(){

        // remove from call log if received
        var patID = localStorage.getItem("patient");

        var fire = new Firebase('https://neverforgotten.firebaseio.com/');
        var patFire = fire.child(patID + "/reminderCreated");
        patFire.set(null);
    }

    function ajaxErrorRem( xhr, status, errorThrown ) {
        alert( "Sorry, there was an ajax problem with new reminder notification!" );
        console.log( "Error: " + errorThrown );
        console.log( "Status: " + status );
        console.dir( xhr );
    }
}());