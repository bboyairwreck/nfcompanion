/**
 * Created by wendykung on 6/1/15.
 */
!(function() {
    var firstName = localStorage.getItem("firstName");
    var lastName = localStorage.getItem("lastName");
    var profile = localStorage.getItem("profile");

    $(document).ready(function() {
        $("#fullName").val(localStorage.getItem("profile"));
        $("#dob").val(localStorage.getItem("dob"));
        $("#phone").val(localStorage.getItem("phone"));
        $("#addressStreet").val(localStorage.getItem("street"));
        $("#addressCity").val(localStorage.getItem("cityState"));
        $("#emergencyName").val(localStorage.getItem("emergencyName"));
        $("#emergencyPhone").val(localStorage.getItem("emergencyPhone"));
        $("#doctor").val(localStorage.getItem("doctor"));
        $("#hospital").val(localStorage.getItem("hospital"));
        $("#blood").val(localStorage.getItem("blood"));
        $("#allergies").text(localStorage.getItem("allergies"));
        $("#meds").text(localStorage.getItem("meds"));
        $("#insurance").val(localStorage.getItem("insurance"));
        $("#ssn").val(localStorage.getItem("ssn"));

        $("#saveProfileButton").click(function() {
            localStorage.setItem("profile", $("#fullName").val());
            localStorage.setItem("dob", $("#dob").val());
            localStorage.setItem("phone", $("#phone").val());
            localStorage.setItem("street", $("#addressStreet").val());
            localStorage.setItem("cityState", $("#addressCity").val());
            localStorage.setItem("emergencyName", $("#emergencyName").val());
            localStorage.setItem("emergencyPhone", $("#emergencyPhone").val());
            localStorage.setItem("doctor", $("#doctor").val());
            localStorage.setItem("hospital", $("#hospital").val());
            localStorage.setItem("blood", $("#blood").val());
            localStorage.setItem("insurance", $("#insurance").val());
            localStorage.setItem("ssn", $("#ssn").val());

            var href = $(this).data("href");
            navWithParams(href);
        });

    });

}());