$(document).ready(function() {
    clearCallLog();

    // Firebase - Check for changes
    var fire = new Firebase('https://neverforgotten.firebaseio.com/');
    var perID = localStorage.getItem("personCompanion");
    var patFire = fire.child(perID + "/callLog");

    patFire.on('child_added', fbCallAdded);
});

function fbCallAdded(snapshot){
    var msg = snapshot.val();

    // remove from call log if received
    clearCallLog();

    navWithParams("call.html");
}

function clearCallLog(){
    // remove from call log if received
    var fire = new Firebase('https://neverforgotten.firebaseio.com/');
    var perID = localStorage.getItem("personCompanion");
    var patFire = fire.child(perID + "/callLog");
    patFire.set(null);
}