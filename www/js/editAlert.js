!(function() {
    var personCompID = localStorage.getItem("personCompanion");
    var personPatID = localStorage.getItem("personPatient");
    var patientName = localStorage.getItem("firstName");

    $(document).ready(function () {
        $("#patientName").text(patientName);

        var myParams = getParams();

        params.pop();

        var action = myParams["action"];

        var reminderID = myParams["ReminderID"];
        var reminderPersonName = myParams["reminderPerson"];
        var reminderNum = myParams["reminderNum"];
        var reminderType = myParams["reminderType"];
        var reminderTime = reminderNum + " " + reminderType;

        $("#alertTime").text(reminderTime);
        $("#alertOption").val(reminderTime);        // selects option

        if (reminderPersonName == "Me") {
            $("#addToMe").addClass('active');
        } else {
            $("#addToPatient").addClass('active');
        }

        $("select.selectRepeat").change(function() {
            var remTime = $("select.selectNot option:selected").text();
            $("#alertTime").text(remTime);
            $("#alertTime").removeClass("promptTime");
        });

        $("#cancelAlert").click(function() {
            var href = $(this).data("href");
            navWithParams(href);
        });

        $("#editAlertButton").click(function() {
            var reminderPerson = "";
            if (reminderPersonName == "Me" && $("#addToPatient").hasClass('active') && $("#addToMe").hasClass('active')) {
                reminderPerson = patientName;
                addParams(reminderPerson);
            } else if (reminderPersonName == patientName && $("#addToMe").hasClass('active') && $("#addToPatient").hasClass('active')) {
                // add alert for comp
                reminderPerson = "Me";
                addParams(reminderPerson);
            } else if ($("#addToMe").hasClass('active')) {
                reminderPerson = "Me";
                editParams(reminderPerson, reminderID);
            } else if ($("#addToPatient").hasClass('active')) {
                reminderPerson = patientName;
                editParams(reminderPerson, reminderID);
            }


            var href = $(this).data("href");
            navWithParams(href);
        });


    });

    function editParams(reminderPerson, reminderID) {
        var alertInfo = [];
        alertInfo["action"] = "editAlert";
        alertInfo["ReminderID"] = reminderID;
        alertInfo["alertPerson"] = reminderPerson;
        var reminderTime = $("#alertTime").text();
        var reminderTimeArr = reminderTime.split(" ");
        //var reminderType = reminderTimeArr[1];
        //if (reminderType.charAt(reminderType.length - 1) != "s") {
        //    reminderType = reminderType + "s";
        //}
        alertInfo["alertNum"] = reminderTimeArr[0];
        alertInfo["alertType"] = reminderTimeArr[1];
        params.push(alertInfo);
    }

    function addParams(reminderPerson) {
        var alertInfo = [];
        alertInfo["action"] = "addAlert";
        alertInfo["alertPerson"] = reminderPerson;
        var reminderTime = $("#alertTime").text();
        var reminderTimeArr = reminderTime.split(" ");
        alertInfo["alertNum"] = reminderTimeArr[0];
        alertInfo["alertType"] = reminderTimeArr[1];
        params.push(alertInfo);
    }





}());
