!(function() {
    var personCompID = localStorage.getItem("personCompanion");
    var personPatID = localStorage.getItem("personPatient");
    var patientName = localStorage.getItem("firstName");

    $(document).ready(function () {
        var myParams = getFirstParam();

        var eventID = myParams["eventID"];

        var url = "http://ericchee.com/neverforgotten/getReminders_Event.php";
        $.ajax(url, {
            dataType: "json",
            data: {
                'n': eventID,
                'patient': personPatID,
                'comp': personCompID
            },
            success: ajaxSuccess,
            error: ajaxError
        });

        $("#remTime").change(function () {
            updateTimeSpan();
        });

        $("#remDate").change(function() {
            updateDateSpan();
        });

        $("select.optionClass").change(function() {
            var selected = $(this).children(":selected").text();
            $(this).prev().children(":first").text(selected);
        });

        $("#addReminderAlert").click(function() {
            updateEvent();

            var href = $(this).data("href");
            navWithParams(href);
        });

        $("#deleteEvent").click(function() {
            clearParams();


            deleteEvent(eventID);

            var href = $(this).data("href");
            navWithParams(href);
        });

        $("#cancelCreate").click(function() {
            while (params.length > 1) {
                params.pop();
            }

            var href = $(this).data("href");
            navWithParams(href);
        });

        $("#doneEditReminder").click(function() {
            editEvent(eventID);
            while (params.length > 1) {
                var update = getParams();
                var action = update["action"];
                if (action == "delete") {
                    var reminderID = update["ReminderID"];
                    deleteAlert(reminderID);
                } else if (action == "addAlert") {
                    var alertNum = update["alertNum"];
                    var alertType = update["alertType"];
                    if (alertType.charAt(alertType.length - 1) != "s") {
                        alertType = alertType + "s";
                    }
                    var personName = update["alertPerson"];
                    if (personName == "Me") {
                        addAlert(eventID, alertNum, alertType, personCompID);
                    } else if (personName == patientName) {
                        addAlert(eventID, alertNum, alertType, personPatID);
                    }
                } else if (action == "editAlert") {
                    var remID = update["ReminderID"];
                    var num = update["alertNum"];
                    var type = update["alertType"];
                    if (type.charAt(type.length - 1) != "s") {
                        type = type + "s";
                    }
                    var person = update["alertPerson"];
                    if (person == "Me") {
                        editAlert(remID, num, type, personCompID);
                    } else if (person == patientName) {
                        editAlert(remID, num, type, personPatID);
                    }
                }
                params.pop();
            }

            var href = $(this).data("href");
            navWithParams(href);
        });
    });

    function editEvent(eventID) {
        var name = $("#remName").val();
        var date = $("#remDate").val();
        var time = $("#remTime").val();
        var datetime = date + " " + time;

        var url = "http://ericchee.com/neverforgotten/updateEvent.php";

        $.ajax(url, {
            dataType : "json",
            data : {
                'n': eventID,
                'datetime': datetime,
                'title': name
            },
            success : editEventSuccess,
            error : editEventError
        });
    }

    function deleteEvent(eventID) {
        var url = "http://ericchee.com/neverforgotten/deleteEvent.php";

        $.ajax(url, {
            dataType : "json",
            data : {
                'n': eventID
            },
            success : deleteEventSuccess,
            error : deleteEventError
        });
    }

    function deleteAlert(reminderID) {
        var url = "http://ericchee.com/neverforgotten/deleteEventReminder.php";

        $.ajax(url, {
            dataType : "json",
            data : {
                'n': reminderID
            },
            success : deleteAlertSuccess,
            error : deleteAlertError
        });
    }

    function editAlert(reminderID, alertNum, alertType, alertPerson) {
        var url = "http://ericchee.com/neverforgotten/updateEventReminder.php";

        $.ajax(url, {
            dataType : "json",
            data : {
                'n': reminderID,
                'num': alertNum,
                'type' : alertType,
                'id' : alertPerson
            },
            success : editAlertSuccess,
            error : editAlertError
        });
    }

    function addAlert(eventID, alertNum, alertType, alertPerson) {
        var url = "http://ericchee.com/neverforgotten/addReminderAlert.php";

        $.ajax(url, {
            dataType : "json",
            data : {
                'event': eventID,
                'num': alertNum,
                'type' : alertType,
                'id' : alertPerson
            },
            success : addAlertSuccess,
            error : addAlertError
        });
    }

    function editEventSuccess(data) {

    }

    function editAlertSuccess(data) {

    }

    function deleteEventSuccess(data) {

    }

    function deleteAlertSuccess(data) {

    }

    function addAlertSuccess(data) {


    }

    function editEventError( xhr, status, errorThrown ) {
        alert("Edit Event Error: " + errorThrown);
        console.log( "Error: " + errorThrown );
        console.log( "Status: " + status );
        console.dir( xhr );
    }

    function editAlertError( xhr, status, errorThrown ) {
        alert("Edit Alert Error: " + errorThrown);
        console.log( "Error: " + errorThrown );
        console.log( "Status: " + status );
        console.dir( xhr );
    }

    function deleteEventError( xhr, status, errorThrown ) {
        alert("Delete Event Error: " + errorThrown);
        console.log( "Error: " + errorThrown );
        console.log( "Status: " + status );
        console.dir( xhr );
    }

    function deleteAlertError( xhr, status, errorThrown ) {
        alert("Delete Alert Error: " + errorThrown);
        console.log( "Error: " + errorThrown );
        console.log( "Status: " + status );
        console.dir( xhr );
    }

    function addAlertError( xhr, status, errorThrown ) {
        alert("Add Alert Error: " + errorThrown);
        console.log( "Error: " + errorThrown );
        console.log( "Status: " + status );
        console.dir( xhr );
    }

    function ajaxError( xhr, status, errorThrown ) {
        alert(errorThrown);
        console.log( "Error: " + errorThrown );
        console.log( "Status: " + status );
        console.dir( xhr );
    }

    function updateEvent() {
        var alertInfo = [];
        alertInfo["action"] = "edit";
        alertInfo["eventName"] = $("#remName").val();
        alertInfo["eventDate"] = $("#remDate").val();
        alertInfo["eventTime"] = $("#remTime").val();
        params.push(alertInfo);
    }

    function ajaxSuccess(data) {
        if (data.length > 0) {
            var eventName = data[0]["EventTitle"];
            var eventDateTime = data[0]["EventTime"];
            var eventDateTimeArr = datetimeFormat(eventDateTime);
            var eventDate  = eventDateTimeArr["date"];
            var eventTime = eventDateTimeArr["time"];

            $("#remName").val(eventName);
            $("#remDate").val(eventDate);
            $("#remTime").val(eventTime);

            updateDateSpan();
            updateTimeSpan();

            for (var i = 0; i < data.length; i++){
                var reminderPersonID = data[i]["PersonID"];
                var reminderPerson = data[i]["PersonFName"];
                if (reminderPersonID == personCompID) {
                    reminderPerson = "Me";
                }
                var reminderNum = data[i]["QuantityNum"];
                var reminderType = data[i]["ReminderType"];
                var reminderID = data[i]["ReminderID"];
                var reminderTime = reminderNum + " " + reminderType;

                if (reminderNum > 0) {
                    var $newAlert = $("#reminderAlertTemplate .template").clone();
                    $newAlert.attr("data-reminder", reminderID);
                    var $alertTime = $newAlert.find(".alertTime");
                    $alertTime.text(reminderTime);
                    var $alertName = $newAlert.find(".alertName");
                    $alertName.text(reminderPerson);
                    $("#alertView").append($newAlert);
                }

            }

            $("div.notClicked").click(function() {
                $(this).css("display", "none");
                $(this).prev().css("display", "block");
                $(this).next().css("padding-left", "10px");
            });

            $("div.hasClicked").click(function() {
                // remove list item
                // store reminderID for future delete in params
                var reminderID = $(this).parent().data("reminder");
                var alertInfo = [];
                alertInfo["action"] = "delete";
                alertInfo["ReminderID"] = reminderID;
                params.push(alertInfo);
                $(this).parent().css("display", "none");
            });

            $(".editReminderAlert").click(function() {
                updateEvent();
                var $reminder = $(this).parents('div.template');
                var reminderID = $reminder.data("reminder");
                var alertInfo = [];
                alertInfo["action"] = "editAlert";
                alertInfo["ReminderID"] = reminderID;
                alertInfo["reminderPerson"] = $reminder.find("span.alertName").text();
                var reminderTime = $reminder.find("label.alertTime").text();
                var reminderTimeArr = reminderTime.split(" ");
                alertInfo["reminderNum"] = reminderTimeArr[0];
                alertInfo["reminderType"] = reminderTimeArr[1];
                params.push(alertInfo);

                var href = $(this).data("href");
                navWithParams(href);
            });

            if (params.length > 1) {
                updateReminder();
            }
        }
    }

    function updateReminder() {
        for (var i = 1; i < params.length; i++) {
            var update = params[i];
            if (update["action"] == "delete") {
                var remID = update["ReminderID"];
                var $deleteLine = $("div").find("[data-reminder='" + remID + "']");
                $deleteLine.css("display", "none");
            } else if (update["action"] == "edit") {
                var eventName = update["eventName"];
                var eventDate  = update["eventDate"];
                var eventTime = update["eventTime"];
                $("#remName").val(eventName);
                $("#remDate").val(eventDate);
                $("#remTime").val(eventTime);
                updateDateSpan();
                updateTimeSpan();
            } else if (update["action"] == "addAlert") {
                var alertNum = update["alertNum"];
                var alertType = update["alertType"];
                var alertTime = alertNum + " " + alertType;
                var alertPerson = update["alertPerson"];
                var $newAlert = $("#reminderAlertTemplate .template").clone();
                var $alertTime = $newAlert.find(".alertTime");
                $alertTime.text(alertTime);
                var $alertName = $newAlert.find(".alertName");
                $alertName.text(alertPerson);
                $("#alertView").append($newAlert);
            } else if (update["action"] == "editAlert") {
                var reminderID = update["ReminderID"];
                var reminderPerson = update["alertPerson"];
                var reminderNum = update["alertNum"];
                var reminderType = update["alertType"];
                var reminderTime = reminderNum + " " + reminderType;
                var $alertLine = $("div").find("[data-reminder='" + reminderID + "']");
                $alertLine.find("label.alertTime").text(reminderTime);
                $alertLine.find("label.alertName").text(reminderPerson);
            }
        }
    }

    function addAlertParams(alertNum, alertType, alertPerson) {
        var alertInfo = [];
        alertInfo["action"] = "addAlert";
        alertInfo["alertNum"] = alertNum;
        alertInfo["alertType"] = alertType;
        alertInfo["alertPerson"] = alertPerson;
        params.push(alertInfo);
    }

    function updateTimeSpan() {
        var time = $("#remTime").val();

        var timeString;
        if (time) {
            var arrTime = time.split(":");

            var hours = arrTime[0];
            var mins = arrTime[1];
            var AMorPM = " AM";

            if (hours.charAt(0) == "0") {
                hours = hours.charAt(1);
            }

            if (parseInt(hours) >= 12) {
                AMorPM = " PM";
            }

            hours = parseInt(hours) % 12;
            if (hours == 0) {
                hours = 12;
            }

            timeString = hours + ":" + mins + AMorPM;
        } else {
            $("#remTime").val("00:00");
            timeString = "12:00 AM";
        }

        $('#remTimeString').text(timeString);
    }

    function updateDateSpan() {
        var date = $("#remDate").val();

        if (date) {
            var dateArr = date.split("-");

            var year = dateArr[0];
            var month = dateArr[1];
            var day = dateArr[2];

            if (month.charAt(0) == "0") {
                month = month.charAt(1);
            }

            if (day.charAt(0) == "0") {
                day = day.charAt(1);
            }

            var monthString = getMonthString(month);
        }

        var dateString = monthString + " " + day + ", " + year;
        $("#remDateString").text(dateString);
    }

}());